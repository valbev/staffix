import React, { Component } from 'react';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import {Switch, Route } from "react-router-dom";
import {connect, Provider} from 'react-redux';
import store from "./store/store";
import MainPage from "./MainPage";
import SignUp from "./SignUp/SignUpPage";
import TripPage from "./Trips";
import { getUSer } from "./store/actions/actions";
class App extends Component {

  componentDidMount() {
    this.props.getUSer();
  }

  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <BrowserRouter>
            <Switch>
              <Route path='/trip' component={TripPage} exact/>
              <Route path='/signUp' component={SignUp} exact/>
              <Route path='/mainPage' component={MainPage} exact/>
              <Route path='/' component={MainPage} exact/>
            </Switch>
          </BrowserRouter>
        </Provider>
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    authUser: state.authReducer.authUser,
  };
}

const mapDispatchToProps = {
  getUSer,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

