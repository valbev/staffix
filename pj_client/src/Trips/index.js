import React, {Component} from 'react';
import './TripPage.css';
import 'react-dual-listbox/lib/react-dual-listbox.css';
import { connect } from "react-redux";
import {
    logOut,
    getUSer,
    addTrip,
    addCategory,
    tripInfo,
    addStuff,
    deleteTrip,
    updateTripStatus,
    addLocation,
    addItemInLocation,
} from "../store/actions/actions";

import { Redirect } from 'react-router-dom'
import HeaderTripPage from "./components/header";
import LeftPanelComponent from "./components/left_panel";
import MainDataComponent from "./components/main_data";
import ModalTrip from "./components/trip_modal/add_trip";
import EditTripModal from "./components/trip_modal/edit_trip";
import LogoutModal from "./components/trip_modal/logout_modal";
import DeleteModalTrip from "./components/trip_modal/delete_trip";
import AddCategoryModal from "./components/trip_modal/add_category";
import AddStuffModal from "./components/trip_modal/add_stuff";

import trip_statuses from "./components/left_panel/constatns";
import AddLocationModal from "./components/trip_modal/add_location";

class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title_trip: '',
            dispatchDate: '',
            startPoint: '',
            destinationPoint: '',
            note: '',
            tripInfo: this.props.selectedTrip,
            addClass: 'page-wrappers',
            Class: 'page-wrappers',
            showModal: false,
            category_title: '',
            tripId: '',
            stuff_title: '',
            location_title: '',
            categoryId: '',
            logged: this.props.authUser,
        }
    }


    componentDidMount() {
        const token = this.props.location.search.split('=')[1];
        if(token) {
            localStorage.setItem('accessToken', token);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevState.tripInfo !== this.props.selectedTrip) {
            this.setState({
                tripInfo: this.props.selectedTrip
            })
        }
    }

    handleChangeInput = (event) => {
        this.setState( {
            [event.target.name]: event.target.value
        });
    };



    handleAddTrip = () => {
        let { title_trip, dispatchDate, startPoint, destinationPoint, note } = this.state;
        let userId = this.props.authUser.data.user_id;
        const planedDispatchDate = dispatchDate;
        this.props.addTrip({
            title_trip,
            startPoint,
            destinationPoint,
            note,
            planedDispatchDate,
            userId,
        })
    };

    handleAddCategory = () => {
         let { category_title } = this.state;
        this.props.addCategory({
            category_title,
        });
    };

    hadleAddStuff = () => {
        let { stuff_title } = this.state;
        let categoryId = this.state.categoryId;
        this.props.addStuff({
            stuff_title,
            categoryId,
        });
    };

    handleAddLocation = () => {
        let { location_title } = this.state;
        this.props.addLocation({
            location_title,
        });
    };

    handleCloseTrip = () => {
        const { tripInfo } = this.state;
        const { cancelled } = trip_statuses;
        this.props.updateTripStatus({travelId: tripInfo.travelid, status: cancelled});
    };

    handleStartTrip = () => {
        const { tripInfo } = this.state;
        const { active } = trip_statuses;
        this.props.updateTripStatus({travelId: tripInfo.travelid, status: active});
    };


    handleChangeTripInfo = (trip) => {
        this.props.tripInfo(trip);
        this.setState({ tripInfo: trip, Class: 'page-wrapper page-wrapper-disable'});
    };

    handleCategoryId = (id) => {
        this.setState({ categoryId: id})
    };

    handleLogout = () => {
        localStorage.removeItem('accessToken');
        this.setState({
            logged: null,
        })
        this.props.logOut();
    };

    renderRedirect = () => {
        return <Redirect to='/' />
    };

    handleDeleteTrip = () => {
        let tripId = this.state.tripInfo.travelid;
        this.props.deleteTrip({
            tripId
        })
        this.setState({ tripInfo: '', Class: 'page-wrapper'});
    };

    handleAddStuffToLocation = (stuffArray) => {
        this.props.addItemInLocation({
            stuffId: stuffArray,
            locationId: this.state.tripInfo.startpoint,
        });
    };

    handleChangeStartLocation = (value) => {
        this.setState({
            startPoint: Number(value.value),
        })
    };

    handleChangeEndLocation = (value) => {
        this.setState({
            destinationPoint: Number(value.value),
        })
    };

    render() {
        const { authUser, userTrips, tripStuff, userLocation, userCategory, itemInLocation, userCategorySecond} = this.props;
        let { title_trip, dispatchDate, startPoint, destinationPoint, note, category_title, stuff_title, location_title, tripInfo, Class, logged } = this.state;
        return (
            <div>
                {logged === null && (this.renderRedirect())}
                <head>
                    <link
                        rel='stylesheet prefetch'
                        href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
                    />
                    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" />
                    <script src="//code.jquery.com/jquery-1.11.1.min.js" />
                </head>

                <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <div className="navbar-header">Stuffix</div>
                    <HeaderTripPage authUser={authUser}/>

                    <LeftPanelComponent
                        itemInLocation={itemInLocation}
                        userCategory={userCategory}
                        userTrips={userTrips}
                        authUser={authUser}
                        tripInfo={tripInfo}
                        tripStuff={tripStuff}
                        location={userLocation}
                        handleChangeTripInfo={this.handleChangeTripInfo}
                        handleCategoryId={this.handleCategoryId}
                    />
                </nav>

                <MainDataComponent
                    handleAddStuffToLocation={this.handleAddStuffToLocation}
                    Class={Class}
                    tripInfo={tripInfo}
                    handleCloseTrip={this.handleCloseTrip}
                    handleStartTrip={this.handleStartTrip}
                    userCategory={userCategory}
                    itemInLocation={itemInLocation}
                />

                <ModalTrip
                    location={userLocation}
                    title_trip={title_trip}
                    startPoint={startPoint}
                    destinationPoint={destinationPoint}
                    note={note}
                    handleChangeEndLocation={(value) => this.handleChangeEndLocation(value)}
                    handleChangeStartLocation={(value) => this.handleChangeStartLocation(value)}
                    dispatchDate={dispatchDate}
                    handleChangeInput={this.handleChangeInput}
                    handleAddTrip={this.handleAddTrip}
                />

               <EditTripModal />

               <LogoutModal handleLogout={this.handleLogout}/>

               <DeleteModalTrip handleDeleteTrip={this.handleDeleteTrip} />

                <AddCategoryModal
                    category_title={category_title}
                    handleChangeInput={this.handleChangeInput}
                    handleAddCategory={this.handleAddCategory}
                />

                <AddStuffModal
                    stuff_title={stuff_title}
                    handleChangeInput={this.handleChangeInput}
                    hadleAddStuff={this.hadleAddStuff}
                />

                <AddLocationModal
                    stuff_title={location_title}
                    handleChangeInput={this.handleChangeInput}
                    handleAddLocation={this.handleAddLocation}
                />

            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        authUser: state.authReducer.authUser,
        userTrips: state.authReducer.userTrips,
        userCategory: state.userStuffReducer.userCategory,
        userCategorySecond : state.userStuffReducer.userCategorySecond,
        tripStuff: state.userStuffReducer.userStuffs,
        selectedTrip: state.tripInfoReducer.tripInfo,
        userLocation: state.locationReducer.location,
        itemInLocation: state.userStuffReducer.itemInLocation,
    };
}

const mapDispatchToProps = {
    logOut,
    addTrip,
    getUSer,
    addCategory,
    tripInfo,
    addStuff,
    deleteTrip,
    updateTripStatus,
    addLocation,
    addItemInLocation,
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
