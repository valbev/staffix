import React from 'react';


const HeaderTripPage = ({authUser}) => {
    return (
        <ul className=" nav navbar-right top-nav">
            <li className="dropdown">
                <a
                    href="#"
                    className="dropdown-toggle"
                    data-toggle="dropdown">
                    { authUser !== null && `${authUser.data.first_name} ${authUser.data.last_name}`}
                </a>
                <ul className="dropdown-menu" id="exit-but">
                    <li> <a data-toggle="modal" data-target="#logoutModal">Выход</a></li>
                </ul>
            </li>
        </ul>
    )
}

export default HeaderTripPage;