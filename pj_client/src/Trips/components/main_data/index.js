import React from 'react';
import Widget from "../../DualList";

import trip_statuses from "../left_panel/constatns";


const MainDataComponent = ({Class, tripInfo, handleCloseTrip, handleStartTrip, userCategory, handleAddStuffToLocation, itemInLocation}) => {
    const month = [
        'Января',
        'Февраля',
        'Марта',
        'Апреля',
        'Мая',
        'Июня',
        'Июля',
        'Августа',
        'Сентября',
        'Октября',
        'Ноября',
        'Декабря'
    ];

    let planedDate = null;
    if(tripInfo !== null) {
        const  d = new Date(tripInfo.planeddispatchdate);
        planedDate = d.getDate().toString().padStart(2, '0') + ' ' + month[d.getMonth()];
    }

    console.log(tripInfo);

    const { planed, active } = trip_statuses;

    return (
        <div id="page-wrapper" className={Class}>
            <div className="container-fluid">
                { tripInfo === null && <p className='Choose-trip'>Choose your trip...</p>}
                { tripInfo &&
                    <div className="row" id="main-info">
                        <div className="container">
                            <div className="row">
                                <h3 className="main-letter">Информация о поездке</h3>
                            </div>
                            <div className="row">
                                <fieldset className="for-panel">
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <div className="form-horizontal">
                                                <label className="col-xs-5 control-label">Название поездки:</label>
                                                <p className="form-control-static">{tripInfo.title}</p>
                                                <label className="col-xs-5 control-label">Дата поездки:</label>
                                                <p className="form-control-static">{planedDate}</p>

                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <div className="form-horizontal">
                                                <label className="col-xs-5 control-label">Откуда:</label>
                                                <p className="form-control-static">{tripInfo.start_point_name}</p>
                                                <label className="col-xs-5 control-label">Куда:</label>
                                                <p className="form-control-static">{tripInfo.destination_point_name}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <label className="control-label">Описание:</label>
                                        <p className="form-control-static">{tripInfo.note}</p>
                                    </div>
                                    {tripInfo.status === planed &&
                                        <button
                                            className="btn btn-default"
                                            data-dismiss="modal"
                                            id="but-redact"
                                            onClick={handleStartTrip}
                                        >
                                            <a href="#">Начать поездку</a>
                                        </button>
                                    }
                                    {tripInfo.status === active &&
                                        <button
                                            className="btn btn-default"
                                            data-dismiss="modal"
                                            id="but-redact"
                                            onClick={handleCloseTrip}
                                        >
                                            <a href="#">Завершить</a>
                                        </button>
                                    }

                                    <button
                                        className="btn btn-default"
                                        data-dismiss="modal"
                                        id="but-redact"
                                    >
                                        <a
                                            href="#"
                                            data-toggle="modal"
                                            data-target="#DeleteTripModal"
                                        >
                                            Удалить
                                        </a>
                                    </button>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                }
            </div>

            {tripInfo !== null && userCategory !== null && itemInLocation !== null &&
                <div className='Widget'>
                    <div className='title-stuff'>
                        <h3 className='stuff'>Общий список вещей</h3>
                        <h3 className='stuff'>Выбранные вещи</h3>
                    </div>
                    <Widget
                        tripInfo={tripInfo}
                        className='DualBox'
                        userCategory={userCategory}
                        handleAddStuffToLocation={handleAddStuffToLocation}
                        itemInLocation={itemInLocation}
                    />
                </div>
            }
        </div>
    )
};

export default MainDataComponent;
