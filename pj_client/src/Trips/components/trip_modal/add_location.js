import React from 'react';

const AddLocationModal = ({handleChangeInput, location_title, handleAddLocation}) => {
    return (
        <div className="modal" id="AddLocationModal" tabIndex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h3>Добавить локацию</h3>
                    </div>
                    <div className="modal-body">
                        <div className="input-group form-group" id="modal-cat">
                            <input type="text"
                                   onChange={handleChangeInput}
                                   name="location_title"
                                   value={location_title}
                                   className="form-control"
                                   placeholder="Название"/>

                        </div>
                        <div className="actionsBtns">
                            <button className="btn btn-default" data-dismiss="modal" id="but-cancel">Отмена</button>
                            <button
                                className="btn btn-default btn-primary"
                                id="submit-ok-category"
                                onClick={handleAddLocation}
                                data-dismiss="modal"
                            >
                                Добавить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default AddLocationModal;