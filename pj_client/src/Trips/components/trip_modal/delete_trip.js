import React from 'react';

const DeleteModalTrip = ({handleDeleteTrip}) => {
  return (
      <div className="modal" id="DeleteTripModal" tabIndex="-1" role="dialog" aria-hidden="true">
          <div className="modal-dialog modal-sm">
              <div className="modal-content">

                  <div className="modal-body">
                      <p> Вы уверены, что хотите удалить поездку? <br/></p>
                      <div className="actionsBtns">

                          <button className="btn btn-default" data-dismiss="modal" id="but-cancel">Отмена</button>
                          <button type="submit"
                                  className="btn btn-default btn-primary"
                                  id="submit-ok"
                                  data-dismiss="modal"
                                  onClick={handleDeleteTrip}
                          >Удалить</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  )
};

export default DeleteModalTrip;