import React from 'react';

const AddStuffModal = ({handleChangeInput, stuff_title, hadleAddStuff}) => {
  return (
      <div className="modal" id="AddStaffModal" tabIndex="-1" role="dialog" aria-hidden="true">
          <div className="modal-dialog">
              <div className="modal-content">
                  <div className="modal-header">
                      <h3>Добавить вещь</h3>
                  </div>
                  <div className="modal-body">
                      <div className="input-group form-group" id="modal-cat">
                          <input type="text"
                                 onChange={handleChangeInput}
                                 name="stuff_title"
                                 value={stuff_title}
                                 className="form-control"
                                 placeholder="Название"/>

                      </div>
                      <div className="actionsBtns">
                          <button className="btn btn-default" data-dismiss="modal" id="but-cancel">Отмена</button>
                          <button
                              className="btn btn-default btn-primary"
                              id="submit-ok-category"
                              onClick={hadleAddStuff}
                              data-dismiss="modal"
                          >
                              Добавить
                          </button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  )
};

export default AddStuffModal;