import React from 'react';

const EditTripModal = () => {
    return (
        <div className="modal" id="EditTripModal" tabIndex="-1" role="dialog" aria-hidden="true">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <h3>Редактировать поездку</h3>
                </div>
                <div className="modal-body-trip">
                    <div className="input-group form-group">
                        <input type="text" className="form-control" placeholder="Название поездки"/>
                    </div>
                    <div className="input-group form-group">

                        <input type="text" className="form-control" placeholder="Откуда"/>

                    </div>
                    <div className="input-group form-group">

                        <input type="text" className="form-control" placeholder="Куда"/>

                    </div>
                    <div className="input-group form-group">

                        <input type="text" className="form-control" placeholder="Описание"/>

                    </div>
                    <div className="input-group form-group">

                        <input type="text" className="form-control" placeholder="Дата отправления"/>

                    </div>
                    <div className="actionsBtns">
                            <button className="btn btn-default" data-dismiss="modal" id="but-cancel">Отмена</button>
                            <button className="btn btn-default btn-primary" data-dismiss="modal" id="submit-ok-trip">Редактировать</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
};

export default EditTripModal;