import React from 'react';


const LogoutModal = ({handleLogout}) => {
    return (
        <div className="modal" id="logoutModal" tabIndex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog modal-sm">
                <div className="modal-content">
                    <div className="modal-body">
                        <p> Вы уверены, что хотите выйти?<br/></p>
                        <div className="actionsBtns">
                            <button className="btn btn-default" data-dismiss="modal" id="but-cancel">Отмена</button>
                            <button onClick={handleLogout} className="btn btn-default btn-primary" data-dismiss="modal" id="submit-ok">Выйти</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default LogoutModal;