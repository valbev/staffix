import React from 'react';

const AddCategoryModal = ({category_title, handleChangeInput, handleAddCategory}) => {
    return (
        <div className="modal" id="AddCategoryModal" tabIndex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h3>Добавить категорию</h3>
                    </div>
                    <div className="modal-body">
                        <div className="input-group form-group" id="modal-cat">
                            <input type="text"
                                   name="category_title"
                                   value={category_title}
                                   onChange={handleChangeInput}
                                   className="form-control"
                                   placeholder="Название категории"/>

                        </div>
                        <div className="actionsBtns">
                            <button className="btn btn-default" data-dismiss="modal" id="but-cancel">Отмена</button>
                            <button
                                className="btn btn-default btn-primary"
                                id="submit-ok-category"
                                onClick={handleAddCategory}
                                data-dismiss="modal"
                            >
                                Добавить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default AddCategoryModal;