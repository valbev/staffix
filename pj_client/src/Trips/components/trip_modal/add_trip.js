import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import '../../TripPage.css';


const ModalTrip = ({
                       title_trip,
                       startPoint,
                       destinationPoint,
                       note,
                       dispatchDate,
                       handleChangeInput,
                       handleAddTrip,
                       location,
                       handleChangeStartLocation,
                       handleChangeEndLocation,
}) => {
    const [trip, setTrip] = useState([]);
    const [secondTrip, setSecondTrip] = useState([]);


    useEffect(() => {
        let newTrip = (location && location.data) ?
            location.data.map((item) => {
                return {
                    value: `${item.locationid}`,
                    label: `${item.title}`
                }
            })
            : []
        setTrip(newTrip);
        setSecondTrip(newTrip);
    }, [location]);

    const handleSelectStartPoint = (value) => {
        handleChangeStartLocation(value)
        const newTrip = trip.filter((item) => {console.log(item, Number(value.value)); return Number(item.value) !== Number(value.value)});
        setSecondTrip(newTrip);
    }

    const handleSelectEndPoint = (value) => {
        handleChangeEndLocation(value);
    }

    return (
        <div className="modal" id="AddTripModal" tabIndex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h3>Добавить поездку</h3>
                    </div>
                    <div className="modal-body-trip">
                        <div className="input-group form-group">
                            <input type="text"
                                   name='title_trip'
                                   value={title_trip}
                                   onChange={handleChangeInput}
                                   className="form-control"
                                   placeholder="Название поездки"/>
                        </div>
                        <Select
                            placeholder={'Откуда'}
                            options={trip}
                            onChange={handleSelectStartPoint}
                        />

                        <Select
                            placeholder={'Куда'}
                            options={secondTrip}
                            onChange={handleSelectEndPoint}
                        />
                        <div className="input-group form-group">
                            <input type="text"
                                   name='note'
                                   value={note}
                                   onChange={handleChangeInput}
                                   className="form-control"
                                   placeholder="Описание"/>
                        </div>
                        <div className="input-group form-group">
                            <input type="text"
                                   name='dispatchDate'
                                   value={dispatchDate}
                                   onChange={handleChangeInput}
                                   className="form-control"
                                   placeholder="Дата отправления"/>
                        </div>
                        <div className="actionsBtns">
                            <button className="btn btn-default" data-dismiss="modal" id="but-cancel">Отмена</button>
                            <button
                                onClick={handleAddTrip}
                                className="btn btn-default btn-primary"
                                id="submit-ok-trip"
                                data-dismiss="modal"
                            >
                                Добавить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default ModalTrip;
