import React from 'react';
import PlanedTrip from "./planed_trips";
import HistoryTrip from "./history_trips";

import trip_status from '../constatns';
import ActiveTrip from "./active_trips";

const MyTripComponent = ({authUser, handleChangeTripInfo, userTrips}) => {
    const { planed, cancelled, active } = trip_status;

    const planedTrip = userTrips && userTrips.data && userTrips.data.filter((trip) => trip.status === planed);
    const cancelledTrip = userTrips && userTrips.data && userTrips.data.filter((trip) => trip.status === cancelled);
    const activeTrip = userTrips && userTrips.data && userTrips.data.filter((trip) => trip.status === active);

    return (
        <li>
            <a
                href="#"
                data-toggle="collapse"
                data-target="#submenu-3"
                className="colored"
                id="main-trips"
            >
                Мои поездки
                <i className="fa fa-fw fa-angle-down pull-right"/>
            </a>
                <ul id="submenu-3" className="collapse show">
                    <li>
                        <a href="#" data-toggle="modal" data-target="#AddTripModal"><i className="fas fa-plus" /> Добавить поездку</a>
                    </li>
                    {userTrips && userTrips.data && <PlanedTrip
                        planedTrip={planedTrip}
                        authUser={authUser}
                        handleChangeTripInfo={handleChangeTripInfo}
                    />}
                    {userTrips && userTrips.data && <ActiveTrip
                        activeTrip={activeTrip}
                        handleChangeTripInfo={handleChangeTripInfo}
                    />}
                    {userTrips && userTrips.data &&<HistoryTrip
                        cancelledTrip={cancelledTrip}
                        authUser={authUser}
                        handleChangeTripInfo={handleChangeTripInfo}
                    />}
                </ul>
            }
        </li>
    )
};

export default MyTripComponent;