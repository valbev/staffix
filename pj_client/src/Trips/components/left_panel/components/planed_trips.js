import React from 'react';

const PlanedTrip = ({handleChangeTripInfo, planedTrip}) => {
  return (
      <li>
          <a
              href="#"
              data-toggle="collapse"
              data-target="#submenu-1"
          >
              Запланированные поездки
              <i className="fa fa-fw fa-angle-down pull-right"/>
          </a>
          <ul
              id="submenu-1"
              className="collapse colored"
          >
              {planedTrip.length > 0 && planedTrip.map((trip) => {
                      return (
                          <li
                              className='planed-trips'
                              key={trip.travelid}
                          >
                              <a
                                  id='planed-trips-link'
                                  value={trip}
                                  onClick={() => handleChangeTripInfo(trip)}
                              >
                                  {trip.title}
                              </a>
                          </li>
                      )
                  }
              )}
          </ul>
      </li>
  )
};

export default PlanedTrip;