import React from 'react';

const ActiveTrip = ({handleChangeTripInfo, activeTrip}) => {
    return (
        <li>
            <a
                href="#"
                data-toggle="collapse"
                data-target="#submenu-6"
            >
                Активные поездки
                <i className="fa fa-fw fa-angle-down pull-right"/>
            </a>
            <ul
                id="submenu-6"
                className="collapse colored"
            >
                {activeTrip.length > 0 && activeTrip.map((trip) => {
                        return (
                            <li
                                className='planed-trips'
                                key={trip.travelid}
                            >
                                <a
                                    id='planed-trips-link'
                                    value={trip}
                                    onClick={() => handleChangeTripInfo(trip)}
                                >
                                    {trip.title}
                                </a>
                            </li>
                        )
                    }
                )}
            </ul>
        </li>
    )
};

export default ActiveTrip;