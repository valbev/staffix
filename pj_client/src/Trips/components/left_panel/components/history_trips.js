import React from 'react';


const HistoryTrip = ({handleChangeTripInfo, cancelledTrip}) => {
    return (
        <li>
            <a
                href="#"
                data-toggle="collapse"
                data-target="#submenu-2"
            >
                История поездок
                <i className="fa fa-fw fa-angle-down pull-right"/>
            </a>
            <ul
                id="submenu-2"
                className="collapse colored"
            >
                {cancelledTrip.length > 0 && cancelledTrip.map((trip) => {
                        return (
                            <li
                                className='planed-trips'
                                key={trip.travelid}
                            >
                                <a
                                    id='planed-trips-link'
                                    value={trip}
                                    onClick={() => handleChangeTripInfo(trip)}
                                >
                                    {trip.title}
                                </a>
                            </li>
                        )
                    }
                )}
            </ul>
        </li>
    )
};

export default  HistoryTrip;