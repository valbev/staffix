import React, {useEffect, useState} from "react";
import MyTripComponent from "./components/trips";

const LeftPanelComponent = ({authUser, location, itemInLocation, tripStuff, handleCategoryId, handleChangeTripInfo, userTrips, userCategory}) => {

    const [currentCategory, setCategory] = useState(userCategory);

    useEffect(() => {
        if(userCategory && itemInLocation) {
            if(itemInLocation && itemInLocation.data) {
                const mutator = [...userCategory.data];
                console.log('category ==============>>>>', JSON.parse(JSON.stringify(mutator)));
                const category =  JSON.parse(JSON.stringify(mutator));
                const newCategory = category.map((item) => {
                    const arr = item.stuff;
                    const filter = itemInLocation.data.map((items) => items.item_id);
                    const filtered = arr.filter((val) => {
                            for (let i = 0; i < filter.length; i++) {
                                if (val.id === filter[i]) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    );
                    item.stuff = filtered;
                    return item;
                })


                console.log(newCategory);
                setCategory(newCategory);
            }
        }

    }, [userCategory, itemInLocation]);


        console.log('currentCategory ===============>>>>>', currentCategory);

        return (
            <div className="collapse navbar-collapse navbar-ex1-collapse">
                <ul className="nav navbar-nav side-nav">
                    <MyTripComponent
                        userTrips={userTrips}
                        authUser={authUser}
                        handleChangeTripInfo={handleChangeTripInfo}
                    />

                    <li>
                        <a
                            href="#"
                            data-toggle="collapse"
                            data-target="#submenu-8"
                            className="colored"
                            id="main-trips"
                            style={{backgroundColor: 'rgba(34, 34, 34, 0.88)'}}
                        >
                            Мои локации
                            <i className="fa fa-fw fa-angle-down pull-right" />
                        </a>

                        <ul
                            id="submenu-8"
                            className="collapse colored"
                        >
                            <a
                                className="dropdown-item"
                                href="#"
                                data-toggle="modal"
                                data-target="#AddLocationModal"
                            >
                                <i className="fas fa-plus"/>
                                Добавить Локацию
                            </a>
                            <div className="dropdown-divider" />
                            {
                                location && location.data && location.data.map((location) => {
                                    return (
                                        <a key={location.locationid} className="dropdown-item" href="#">{location.title}</a>
                                    )
                                })
                            }
                            <div className="dropdown-menu">
                                <a className="dropdown-item" href="#"><i className="fa fa-trash-alt"/>Удалить Локацию</a>
                            </div>
                        </ul>
                    </li>

                    <li>
                        <a
                            href="#"
                            data-toggle="collapse"
                            data-target="#submenu-4"
                            className="colored"
                            id="main-trips"
                            style={{backgroundColor: 'rgba(34, 34, 34, 0.88)'}}
                        >
                            Категории вещей
                            <i className="fa fa-fw fa-angle-down pull-right" /></a>
                         <ul id="submenu-4" className="collapse">
                            <li>
                                <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#AddCategoryModal"
                                    style={{backgroundColor: 'rgba(34, 34, 34, 0.88)'}}
                                >
                                    <i className="fas fa-plus" />
                                    Добавить категорию
                                </a>
                            </li>
                            <li>
                                <a
                                    href="#"
                                    data-toggle="collapse"
                                    data-target="#submenu-5"
                                    style={{backgroundColor: 'rgba(34, 34, 34, 0.88)'}}
                                >
                                    Мои категории
                                    <i className="fa fa-fw fa-angle-down pull-right" />
                                </a>

                                <ul
                                    id="submenu-5"
                                    className="collapse colored"
                                >
                                    {currentCategory && currentCategory.map((category) => {
                                            return (
                                                <li
                                                    id="non-pad"
                                                    key={category.category_id}
                                                >
                                                    <button type="button"
                                                            className="btn btn-secondary dropdown-toggle"
                                                            id="drop-btn"
                                                            data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false"
                                                            value={category}
                                                            onClick={() => handleCategoryId(category.category_id)}>
                                                        {category.title}
                                                    </button>
                                                    <div className="dropdown-menu">
                                                        <a className="dropdown-item" href="#"><i className="fa fa-trash-alt"/>Удалить категорию</a>
                                                        <a
                                                            className="dropdown-item"
                                                            href="#"
                                                            data-toggle="modal"
                                                            data-target="#AddStaffModal"
                                                        >
                                                            <i className="fas fa-plus"/>
                                                            Добавить вещь
                                                        </a>
                                                        <div className="dropdown-divider" />
                                                        { category.stuff && category.stuff.map((stuff) =>
                                                            <a key={stuff.id} className="dropdown-item" href="#">{stuff.stuff_name}</a>
                                                        )}
                                                    </div>
                                                </li>
                                            )

                                        }

                                    )}

                                </ul>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        )

}

export default LeftPanelComponent;