import React from 'react';
import DualListBox from 'react-dual-listbox';
import './TripPage.css';

class Widget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: [],
            stuffs:[],
            category: [],
        };
    }

    componentDidMount() {
        this.setState({
            category: this.props.userCategory.data,
        })
        const array = [];
        if(this.props.itemInLocation && this.props.itemInLocation.data) {
           const mutator = [...this.props.userCategory.data];
           const category =  JSON.parse(JSON.stringify(mutator));
           const newValue = category.map((item) => {
                const arr = item.stuff;
                const filter = this.props.itemInLocation.data.map((items) => items.item_id);
               const filteredOptions = arr.filter((val) => {
                       for (let i = 0; i < filter.length; i++) {
                           if (val.id === filter[i]) {
                               return false;
                           }
                       }
                       return true;
                   }
               );

               const filteredSelected = arr.filter((val) => {
                       for (let i = 0; i < filter.length; i++) {
                           if (val.id === filter[i]) {
                               return true;
                           }
                       }
                       return false;
                   }
               );

               array.push(filteredSelected);
               item.selectedStuff = filteredSelected;
                item.stuff = filteredOptions;
                return item;
            })

            const newArray = [];
            const selected = newValue.map((item) => {
                return item.selectedStuff.map((stuff) => {
                    newArray.push(String(stuff.id));
                });
            })

            this.setState({
                category: newValue,
                stuffs: newArray,
            })
        }
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        let selectedStuff = [];
        if(prevState !== this.state.selected) {
            const currentStuffId = this.state.selected.map((item) => {
                return Number(item);
            })
            selectedStuff = currentStuffId;
            this.props.handleAddStuffToLocation(selectedStuff)
        }
    }

    onChange = (selected) => {
        this.setState({ stuffs: selected });
    };


    render() {
        const stuff = this.props.userCategory.data;
        const { stuffs } = this.state;
        let options = stuff ? stuff.map((items, index) => {
           let label = `${items.title}`;
           let optionsArray = items.stuff.map((stuff) => {
               let second = {
                           value: stuff.stuff_name ? `${stuff.id}` : '',
                           label: stuff.stuff_name ? `${stuff.stuff_name}` : '',
               };
                 return  Object.assign({}, second);
               });

           return {
             label: label,
             options: optionsArray
           };
        }) : [];

        return (
            <DualListBox className="DualBox"
                canFilter
                options={options}
                selected={stuffs}
                onChange={this.onChange}
            />
        );
    }

}

export default Widget;