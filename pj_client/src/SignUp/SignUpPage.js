import React, { Component } from 'react';
import { signUp } from "../store/actions/actions";
import { Redirect } from 'react-router-dom'
import {connect} from "react-redux";
import { Link } from "react-router-dom";
import './SignUpPage.css';
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import CustomAlert from "../MainPage/components/custom_alert";



const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />
}

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            second_name:'',
            name: '',
            email: '',
            password:'',
            sign_up: '',
            openError: true,
        }
    }

    handleCloseSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({
            openError: false,
        })
    };

    handlerInputChange = (event) => {
        this.setState( {
            [event.target.name]: event.target.value,
            sign_up:'',
        });
    };

    renderRedirect = () => {
        return <Redirect to='/' />
    };

    handleSignUp = () => {
        let email = this.state.email.replace(/^\s+|\s+$/g, '');
        let name = this.state.name.replace(/^\s+|\s+$/g, '');
        let second_name = this.state.second_name.replace(/^\s+|\s+$/g, '');
        let password = this.state.password.replace(/^\s+|\s+$/g, '');
        if (email && name.length && second_name.length && password.length) {
            this.props.signUp({
                second_name,
                name,
                email,
                password,
            });
            this.setState({second_name:'', name:'', email: '', password: '', openError: true});
        } else {
            alert('Заполните все поля');
        }

    };

    render() {
        let { second_name, name, email, password, openError } = this.state;
        const googleError = this.props.error === 'This email is already registered with google';
        const allReadyExist = this.props.error === 'User with that EMAIL already exist';
        return (
            <div>
                <div id="body-sign-up">
                <div className="sign-up-container">
                    <div className="d-flex justify-content-center h-100">
                        <div className="card">
                            <div className="card-header">
                                <h3>Регистрация</h3>
                            </div>
                            <div className="card-body">
                                    <div className="input-group form-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="fa fa-user" /></span>
                                        </div>
                                        <input name="second_name"
                                               value={second_name}
                                               onChange={this.handlerInputChange}
                                               type="text"
                                               className="form-control"
                                               placeholder="Фамилия"/>

                                    </div>
                                    <div className="input-group form-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="fa fa-user" /></span>
                                        </div>
                                        <input type="text"
                                               name="name"
                                               value={name}
                                               onChange={this.handlerInputChange}
                                               className="form-control"
                                               placeholder="Имя"/>

                                    </div>
                                    <div className="input-group form-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="fa fa-user" /></span>
                                        </div>
                                        <input type="text"
                                               name="email"
                                               value={email}
                                               onChange={this.handlerInputChange}
                                               className="form-control"
                                               placeholder="Email"/>

                                    </div>
                                    <div className="input-group form-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="fa fa-key" /></span>
                                        </div>
                                        <input type="password"
                                               name="password"
                                               value={password}
                                               onChange={this.handlerInputChange}
                                               className="form-control"
                                               placeholder="Пароль"/>
                                    </div>
                                    <div className="bt-group">
                                        <div className="but-group">{(this.props.signup.user === 'success') && <p>*Вы успешно зарегистрировались!</p>}</div>
                                        <div className="but-group">{(this.props.signup.user === 'error') && <p>*Пользователь с таким Email уже существует!</p>}</div>
                                        <div className="actionsBtns">
                                            <button  className="btn float-right sign-up-btn"> <Link to="/mainPage" className="link-to-main-page">Назад</Link></button>
                                            <button onClick={this.handleSignUp} className="btn float-right sign-up-btn">Зарегистрироваться</button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <Snackbar open={googleError && openError} autoHideDuration={6000} onClose={this.handleCloseSnackBar}>
                        <Alert onClose={this.handleCloseSnackBar} severity="error">
                            Вы уже разегистрированы с помощбю Google, <a href="http://localhost:3001/api/v1/auth/google"
                                                                         style={{color: '#fff', textDecoration: 'underline', cursor: 'pointer'}}
                        >
                            нажмите, чтобы Войти
                        </a>
                        </Alert>
                    </Snackbar>
                    {allReadyExist && openError &&
                        <CustomAlert
                            text={'Пользователь с такими данными уже существует.'}
                            linkText={'Назад'}
                            linkTo={'/'}
                            handleCloseSnackBar={this.handleCloseSnackBar}/>
                    }
                </div>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return {
        login: state.authReducer,
        error: state.signUpReducer.error,
        signup: state.signUpReducer,
    };
}

const mapDispatchToProps = {
    signUp,
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
