import { fork, all } from 'redux-saga/effects';
import loginSaga from "./loginSaga";
import checkUserSaga from "./checkUserSaga";
import logoutSaga from "./logoutSaga";
import signupSaga from "./signupSaga";
import addTripSaga from "./addTripSaga";
import getUserStuffSaga from "./getUserStuffsSaga";
import addCategorySaga from "./addCategorySaga";
import addStuffSaga from "./addStuffSaga";
import deleteTripSaga from "./deleteTripSaga";
import getTripSaga from "./getTripSaga";
import getCategorySaga from "./getCategorySaga";
import updateTripStatusSaga from "./updateTripStatusSaga";
import addLocationSaga from "./addLocationSaga";
import getLocationSaga from "./getLocationSaga";
import putItemLocationSaga from "./putItemLocationSaga";
import getItemInLocationSaga from "./getItemInLocationSaga";

export default function* rootSaga() {
    yield all([
        fork(loginSaga),
        fork(checkUserSaga),
        fork(logoutSaga),
        fork(signupSaga),
        fork(addTripSaga),
        fork(getUserStuffSaga),
        fork(addCategorySaga),
        fork(getTripSaga),
        fork(getCategorySaga),
        addStuffSaga(),
        fork(deleteTripSaga),
        fork(updateTripStatusSaga),
        fork(addLocationSaga),
        fork(getLocationSaga),
        fork(putItemLocationSaga),
        fork(getItemInLocationSaga),
    ]);
}