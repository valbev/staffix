import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    ADD_TRIP_ERROR,
    ADD_TRIP_REQUEST,
    GET_TRIP_REQUEST,
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(ADD_TRIP_REQUEST, workerSaga);
}

function* workerSaga(action) {
        try {
            const token = localStorage.getItem('accessToken');
            const headers = {
                'Authorization': token && `${token}`,
                'Content-Type': 'application/json'
            };
            yield call(axios, '/api/v1/trips', {
                method: 'post',
                headers: headers,
                data: {
                    title: action.payload.title_trip,
                    planedDispatchDate: action.payload.planedDispatchDate,
                    startPoint: action.payload.startPoint,
                    destinationPoint: action.payload.destinationPoint,
                    note: action.payload.note,
                    user_id: action.payload.userId,
                }
            });
            yield  put({type: GET_TRIP_REQUEST});
    } catch (e) {
        yield put({ type: ADD_TRIP_ERROR, payload: e.response });
    }
}