import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios';
import {LOG_IN_SUCCESS, LOG_IN, LOG_IN_ERROR} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(LOG_IN, workerSaga);
}

function* workerSaga(action) {
    const token = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': token && `${token}`,
        'Content-Type': 'application/json'
    };
    try {
        const { data } = yield call(axios, '/api/v1/auth/signin', {
            method: 'post',
            headers: headers,
            data: {
                email: action.payload.email,
                password: action.payload.password,
            },
        });
        yield  put({type: LOG_IN_SUCCESS, payload: data});
        localStorage.setItem('accessToken', data.data.token);
    } catch (e) {
        yield put({ type: LOG_IN_ERROR, payload: e.response.data.error });
    }
}
