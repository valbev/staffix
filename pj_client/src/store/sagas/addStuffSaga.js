import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    ADD_STUFF_SUCCESS,
    ADD_STUFF_ERROR,
    ADD_STUFF_REQUEST,
    GET_USER_REQUEST, GET_TRIP_STUFF_REQUEST, GET_CATEGORY_REQUEST,
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(ADD_STUFF_REQUEST, workerSaga);
}

function* workerSaga(action) {
    try {
        const token = localStorage.getItem('accessToken');
        const headers = {
            'Authorization': token && `${token}`,
            'Content-Type': 'application/json'
        };
        const { data } = yield call(axios, '/api/v1/stuff', {
            method: 'post',
            headers: headers,
            data: {
                title: action.payload.stuff_title,
                category_id: action.payload.categoryId,
            }
        });
        yield put({type: ADD_STUFF_SUCCESS, payload: data});
        yield put({ type: GET_USER_REQUEST });
        yield put({ type: GET_TRIP_STUFF_REQUEST, payload: action.payload.tripId});
        yield put({ type: GET_CATEGORY_REQUEST });
    } catch (e) {
        yield put({ type: ADD_STUFF_ERROR, payload: e.response });
    }
}