import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    ADD_LOCATION_SUCCESS,
    GET_LOCATION_REQUEST,
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(GET_LOCATION_REQUEST, workerSaga);
}

function* workerSaga(action) {
    try {
        const token = localStorage.getItem('accessToken');
        const headers = {
            'Authorization': token && `${token}`,
            'Content-Type': 'application/json'
        };
        const { data } = yield call(axios, '/api/v1/location', {
            method: 'get',
            headers: headers,
        });

        yield  put({type: ADD_LOCATION_SUCCESS, payload: data});
    } catch (e) {
        console.log(e);
    }
}