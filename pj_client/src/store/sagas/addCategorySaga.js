import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    ADD_CATEGORY_SUCCESS,
    ADD_CATEGORY_ERROR,
    ADD_CATEGORY_REQUEST,
    GET_TRIP_STUFF_REQUEST, GET_CATEGORY_REQUEST,
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(ADD_CATEGORY_REQUEST, workerSaga);
}

function* workerSaga(action) {
    try {
        const token = localStorage.getItem('accessToken');
        const headers = {
            'Authorization': token && `${token}`,
            'Content-Type': 'application/json'
        };
        const { data } = yield call(axios, '/api/v1/category', {
            method: 'post',
            headers: headers,
            data: {
                title: action.payload.category_title,
            }
        });
        yield  put({type: ADD_CATEGORY_SUCCESS, payload: data});
        yield  put({type: GET_CATEGORY_REQUEST});
        //yield  put({ type: GET_USER_REQUEST });
        yield  put({ type: GET_TRIP_STUFF_REQUEST, payload: action.payload.tripId});
    } catch (e) {
        yield put({ type: ADD_CATEGORY_ERROR, payload: e.response });
    }
}