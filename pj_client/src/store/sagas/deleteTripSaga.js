import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    DELETE_TRIP_SUCCESS,
    DELETE_TRIP_ERROR,
    DELETE_TRIP_REQUEST,
    GET_TRIP_REQUEST,
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(DELETE_TRIP_REQUEST, workerSaga);
}

function* workerSaga(action) {
    try {
        const token = localStorage.getItem('accessToken');
        const headers = {
            'Authorization': token && `${token}`,
            'Content-Type': 'application/json'
        };
        const { data } = yield call(axios, `/api/v1/trips`, {
            method: 'delete',
            headers: headers,
            data: {
                tripId: action.payload.tripId,
            },
        });
        yield  put({type: DELETE_TRIP_SUCCESS, payload: data});
        yield  put({type: GET_TRIP_REQUEST});
        //yield  put({ type: GET_USER_REQUEST });
    } catch (e) {
        yield put({ type: DELETE_TRIP_ERROR, payload: e.response });
    }
}