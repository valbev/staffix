import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    GET_TRIP_ERROR,
    GET_TRIP_REQUEST,
    ADD_TRIP_SUCCESS,
    ADD_TRIP_ERROR,
    SELECTED_TRIP_REQUEST,
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(GET_TRIP_REQUEST, workerSaga);
}

function* workerSaga() {
    try {
        const token = localStorage.getItem('accessToken');
        const headers = {
            'Authorization': token && `${token}`,
            'Content-Type': 'application/json'
        };
        const {data} = yield call(axios, '/api/v1/trips', {
            method: 'get',
            headers: headers,
        });

        yield  put({type: ADD_TRIP_SUCCESS, payload: data});

    } catch (e) {
        yield put({ type: GET_TRIP_ERROR, payload: e.response });
        yield put({ type: ADD_TRIP_ERROR, payload: e.response });
    }
}