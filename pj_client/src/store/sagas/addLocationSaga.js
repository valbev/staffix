import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    ADD_LOCATION_SUCCESS,
    ADD_LOCATION_ERROR,
    ADD_LOCATION_REQUEST, GET_LOCATION_REQUEST,
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(ADD_LOCATION_REQUEST, workerSaga);
}

function* workerSaga(action) {
    try {
        const token = localStorage.getItem('accessToken');
        const headers = {
            'Authorization': token && `${token}`,
            'Content-Type': 'application/json'
        };
        const { data } = yield call(axios, '/api/v1/location', {
            method: 'post',
            headers: headers,
            data: {
                title: action.payload.location_title,
            }
        });

        yield put({type: GET_LOCATION_REQUEST });

    } catch (e) {
        yield put({ type: ADD_LOCATION_ERROR, payload: e.response });
    }
}