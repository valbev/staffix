import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    UPDATE_TRIP_STATUS_REQUEST,
    GET_TRIP_REQUEST,
    SELECTED_TRIP_REQUEST,
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(UPDATE_TRIP_STATUS_REQUEST, workerSaga);
}

function* workerSaga(action) {
    try {
        const token = localStorage.getItem('accessToken');
        const headers = {
            'Authorization': token && `${token}`,
            'Content-Type': 'application/json'
        };
        yield call(axios, `/api/v1/trips/${action.payload.travelId}/${action.payload.status}`, {
            method: 'put',
            headers: headers,
        });
        yield  put({type: GET_TRIP_REQUEST});

        const { data } = yield call(axios, '/api/v1/trips', {
            method: 'get',
            headers: headers,
        });

        yield put({type: SELECTED_TRIP_REQUEST, payload: data})

    } catch (e) {
        console.log('error', e);
    }
}