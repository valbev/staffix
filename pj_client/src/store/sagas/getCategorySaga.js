import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    GET_CATEGORY_REQUEST,
    GET_CATEGORY_SUCCESS,
    GET_CATEGORY_ERROR, GET_ITEM_LOCATION_REQUEST,
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(GET_CATEGORY_REQUEST, workerSaga);
}

function* workerSaga(action) {
    try {
        const token = localStorage.getItem('accessToken');
        const headers = {
            'Authorization': token && `${token}`,
            'Content-Type': 'application/json'
        };
        const { data } = yield call(axios, '/api/v1/category', {
            method: 'get',
            headers: headers,
        });
        yield  put({type: GET_CATEGORY_SUCCESS, payload: data});

        yield  put({type: GET_ITEM_LOCATION_REQUEST });

    } catch (e) {
        yield put({ type: GET_CATEGORY_ERROR, payload: e.response });
    }
}