import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    GET_CATEGORY_REQUEST,
    GET_TRIP_REQUEST,
    GET_USER_REQUEST,
    GET_USER_SUCCESS,
    GET_LOCATION_REQUEST, GET_ITEM_LOCATION_REQUEST
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(GET_USER_REQUEST, workerSaga);
}

function* workerSaga() {
    try {
        const token = localStorage.getItem('accessToken');
        const headers = {
            'Authorization': token && `${token}`,
            'Content-Type': 'application/json'
        };

        const {data} = yield call(axios, '/api/v1/', {
            method: 'get',
            headers: headers,
        });
        yield  put({type: GET_USER_SUCCESS, payload: data});
        yield  put({type: GET_TRIP_REQUEST});
        yield  put({type: GET_CATEGORY_REQUEST});
        yield  put({type: GET_LOCATION_REQUEST});
    } catch(err) {
        yield  put({type: GET_USER_SUCCESS, payload: null});
    }

}