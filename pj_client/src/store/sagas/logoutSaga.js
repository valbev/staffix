import { takeEvery, put } from "redux-saga/effects";
import {LOG_OUT_REQUEST, LOG_OUT_SUCCESS} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(LOG_OUT_REQUEST, workerSaga);
}

function* workerSaga() {

    yield  put({type: LOG_OUT_SUCCESS});
}