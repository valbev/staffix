import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {SIGN_UP_REQUEST, SIGN_UP_SUCCESS, SIGN_UP_ERROR} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(SIGN_UP_REQUEST, workerSaga);
}

function* workerSaga(action) {
    try {
        const { data } = yield call(axios, '/api/v1/auth/signup', {
            method: 'post',
            data: {
                first_name: action.payload.name,
                last_name: action.payload.second_name,
                email: action.payload.email,
                password: action.payload.password,
            }
        });
        yield put({type: SIGN_UP_SUCCESS, payload: data});
    } catch (err) {
        yield put({type: SIGN_UP_ERROR, payload: err.response.data.error });
    }

}