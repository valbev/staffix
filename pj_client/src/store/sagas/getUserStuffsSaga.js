import { takeEvery, call, put } from "redux-saga/effects";
import axios from 'axios'
import {
    GET_TRIP_STUFF_ERROR,
    GET_TRIP_STUFF_REQUEST,
    GET_TRIP_STUFF_SUCCESS
} from "../constants/actions-type";

export default function* watcherSaga() {
    yield takeEvery(GET_TRIP_STUFF_REQUEST, workerSaga);
}

function* workerSaga(action) {
    try {
        const { data } = yield call(axios, '/users/getstuff', {
            method: 'post',
            data: {
                tripId: action.payload,
            }
        });
        yield  put({ type: GET_TRIP_STUFF_SUCCESS, payload: data });
    } catch (e) {
        yield put({ type: GET_TRIP_STUFF_ERROR, payload: e });
    }
}