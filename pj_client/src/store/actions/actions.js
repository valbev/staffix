import {
    LOG_IN,
    LOG_OUT_REQUEST,
    SIGN_UP_REQUEST,
    GET_USER_REQUEST,
    GET_TRIP_STUFF_REQUEST,
    ADD_TRIP_REQUEST,
    ADD_CATEGORY_REQUEST,
    SELECTED_TRIP_SUCCESS,
    ADD_STUFF_REQUEST,
    DELETE_TRIP_REQUEST,
    UPDATE_TRIP_STATUS_REQUEST,
    ADD_LOCATION_REQUEST,
    ADD_ITEM_LOCATION_REQUEST,
    GET_ITEM_LOCATION_REQUEST,
} from "../constants/actions-type";

export function logIn(loginData) {
    return {
        type: LOG_IN,
        payload: loginData,
    }
}

export function signUp(signUpData) {
    return {
        type: SIGN_UP_REQUEST,
        payload: signUpData,
    }
}
export function logOut() {
    return {
        type: LOG_OUT_REQUEST,
    }
}
export function getUSer(user) {
    return {
        type: GET_USER_REQUEST,
        payload: user,
    }
}

export function getUserStuff(data) {
    return {
        type: GET_TRIP_STUFF_REQUEST,
        payload: data,
    }
}
export function addTrip(addData) {
    return {
        type: ADD_TRIP_REQUEST,
        payload: addData,
    }
}

export function updateTripStatus(id) {
    return {
        type: UPDATE_TRIP_STATUS_REQUEST,
        payload: id,
    }
}

export function addCategory(addData) {
    return {
        type: ADD_CATEGORY_REQUEST,
        payload: addData,
    }
}

export function addStuff(addData) {
    return {
        type: ADD_STUFF_REQUEST,
        payload: addData,
    }
}

export function tripInfo(data) {
    return {
        type: SELECTED_TRIP_SUCCESS,
        payload: data,
    }
}
export function deleteTrip(data) {
    return {
        type: DELETE_TRIP_REQUEST,
        payload: data
    }
}

export function addLocation(data) {
    return {
        type: ADD_LOCATION_REQUEST,
        payload: data
    }
}

export function addItemInLocation(data) {
    return {
        type: ADD_ITEM_LOCATION_REQUEST,
        payload: data
    }
}

export function getItemInLocation(data) {
    return {
        type: GET_ITEM_LOCATION_REQUEST,
        payload: data
    }
}


