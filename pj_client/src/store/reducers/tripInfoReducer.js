import {
    SELECTED_TRIP_SUCCESS,
    SELECTED_TRIP_ERROR,
    SELECTED_TRIP_REQUEST,
} from "../constants/actions-type";


const initialState = {
    tripInfo: null,
    tripStuff: null,
    error: null,
};

const tripInfoReducer = (state = initialState, action) => {
    switch (action.type) {
        case SELECTED_TRIP_SUCCESS:
            return {
                ...state,
                tripInfo: action.payload,
            };
        case SELECTED_TRIP_ERROR:
            return {
                ...state,
                error: 'Not Found'
            };
        case SELECTED_TRIP_REQUEST:
            const prevState = {...state};
            const newState = action.payload.data;
            const currentTravel = newState.find((travel) => travel.travelid === prevState.tripInfo.travelid);
            return {
                ...state,
                tripInfo: currentTravel,
            };
        default: return state;
    }
};

export default tripInfoReducer;