import {combineReducers} from 'redux';
import authReducer from "./loginReducer";
import signUpReducer from "./signupReducer";
import userStuffReducer from "./userStuffReducer";
import tripInfoReducer from "./tripInfoReducer";
import locationReducer from "./locationReducer";

const allReducers = combineReducers({
    authReducer: authReducer,
    signUpReducer : signUpReducer,
    userStuffReducer: userStuffReducer,
    tripInfoReducer : tripInfoReducer,
    locationReducer: locationReducer,
});

export default allReducers;