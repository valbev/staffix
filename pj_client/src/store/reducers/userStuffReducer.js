import {
    GET_TRIP_STUFF_ERROR,
    GET_TRIP_STUFF_SUCCESS,
    GET_CATEGORY_SUCCESS,
    GET_CATEGORY_ERROR,
    GET_ITEM_LOCATION_SUCCESS,
} from "../constants/actions-type";


const initialState = {
    userCategory: null,
    userCategorySecond: null,
    userStuffs: null,
    error: null,
    itemInLocation: null,
};

const userStuffReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_TRIP_STUFF_SUCCESS:
            return {
                ...state,
                userStuffs: action.payload,
            };
        case GET_TRIP_STUFF_ERROR:
            return {
                ...state,
                error: 'Not Found'
            };
        case GET_CATEGORY_SUCCESS:
            return {
                ...state,
                userCategory: action.payload,
                userCategorySecond: action.payload,
            };
        case GET_CATEGORY_ERROR:
            return {
                ...state,
                userCategory: null,
                userCategorySecond: null,
            };
        case GET_ITEM_LOCATION_SUCCESS:
            return {
                ...state,
                itemInLocation: action.payload,
            };

        default: return state;
    }
};

export default userStuffReducer;