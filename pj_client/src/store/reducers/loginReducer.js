import {
    ADD_TRIP_SUCCESS,
    GET_USER_ERROR,
    GET_USER_SUCCESS,
    LOG_IN_ERROR,
    LOG_IN_SUCCESS,
    LOG_OUT_SUCCESS,
    ADD_TRIP_ERROR,
} from "../constants/actions-type";


const initialState = {
    userTrips: null,
    authUser:null,
    error: null
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOG_IN_SUCCESS:
            return {
                ...state,
                authUser: action.payload,
                userTrips: action.payload.trips,
                error: null
            };
        case LOG_IN_ERROR:
            return {
                ...state,
                error: action.payload,
            };
        case LOG_OUT_SUCCESS:
            return {
                ...state,
                authUser: null,
                error: null,
            };
        case GET_USER_SUCCESS:
            return {
                ...state,
                authUser: action.payload,
            };
        case GET_USER_ERROR:
            return {
                ...state,
                authUser: null,
                error: 'Not Found'
            };
        case ADD_TRIP_SUCCESS:
            return {
                ...state,
                userTrips: action.payload,
            };
        case ADD_TRIP_ERROR:
            return {
                ...state,
                userTrips: null,
            };

        default: return state;
    }
};

export default authReducer;
