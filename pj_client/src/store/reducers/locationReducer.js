import {
    ADD_LOCATION_SUCCESS,
    ADD_LOCATION_ERROR,
    ADD_LOCATION_REQUEST,
} from "../constants/actions-type";


const initialState = {
    location: null,
    error: null,
};

const locationReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_LOCATION_SUCCESS:
            return {
                ...state,
                location: action.payload,
            };
        case ADD_LOCATION_ERROR:
            return {
                ...state,
                error: 'Not Found'
            };
        default: return state;
    }
};

export default locationReducer;