import {
    SIGN_UP_ERROR,
    SIGN_UP_SUCCESS,
} from "../constants/actions-type";

const initialState = {
    user: null,
    error: null
};

const signUpReducer = (state = initialState, action) => {
    switch (action.type) {
        case SIGN_UP_SUCCESS:
            return {
                ...state,
                user: action.payload,
                error: null
            };
        case SIGN_UP_ERROR:
            return {
                ...state,
                error: action.payload

            };
        default: return state;
    }
};


export default signUpReducer;
