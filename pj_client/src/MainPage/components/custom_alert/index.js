import React from 'react';
import {Link} from "react-router-dom";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "./alert";

const CustomAlert = ({handleCloseSnackBar, text, linkText, linkTo}) => {
    return (
        <Snackbar open={true} autoHideDuration={6000} onClose={handleCloseSnackBar}>
            <Alert onClose={handleCloseSnackBar} severity="error">
                {text} <Link to={linkTo} style={{color: '#fff', textDecoration: 'underline'}}>{linkText}</Link>
            </Alert>
        </Snackbar>
    )
};

export default CustomAlert;