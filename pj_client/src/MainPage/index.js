import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { logIn } from "../store/actions/actions"
import {connect} from "react-redux";
import { Redirect } from 'react-router-dom';
import './MainPage.css';
import googleIcon from '../stickeroid_5bf557921a4bc.png'
import CustomAlert from "./components/custom_alert";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password:'',
            passwordError: this.props.error,
            openError: true,
        }
    }

    renderRedirect = () => <Redirect to='/trip' />

    handleCloseSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({
            openError: false,
        })
    };

    handleLogIn = () => {
        let { email, password } = this.state;
        if(email.length) {
            this.props.logIn({
                email,
                password,
            });
            this.setState({
                openError: true,
            })
        }};

    handlerInputChange = (event) => {
      this.setState( {
          [event.target.name]: event.target.value
      });
    };

    render() {
        let logged = this.props.authUser;
        let { email, password, openError } = this.state;
        const dataError = this.props.error === 'The password you provided is incorrect';
        const userDoesNotExist = this.props.error === 'User with this email does not exist';
        return (
            <div>
                {(logged !== '' && logged !== null) && this.renderRedirect()}
                <div id="body-main-page">
                    <div className="main-page-container">
                        <div className="d-flex justify-content-center h-100">
                            <div className="card" id="main-page-card">
                                <div className="card-header">
                                    <h3>Вход</h3>
                                </div>
                                <div className="card-body">
                                        <div className="input-group form-group">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text"><i className="fa fa-user"/></span>
                                            </div>
                                            <input
                                                onChange={this.handlerInputChange}
                                                value={email}
                                                name="email"
                                                type="email"
                                                className="form-control"
                                                placeholder="Email"
                                            />

                                        </div>
                                        <div className="input-group form-group">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text"><i className="fa fa-key" /></span>
                                            </div>
                                            <input
                                                onChange={this.handlerInputChange}
                                                value={password}
                                                type="password"
                                                name="password"
                                                className="form-control"
                                                placeholder="Пароль"
                                            />
                                        </div>
                                        <div className="form-group-two">
                                             <button onClick={this.handleLogIn} type="submit" className="btn float-right login_btn"> Войти </button>
                                        </div>
                                        <div className="form-group-two">
                                            <a className='google-button' href="http://localhost:3001/api/v1/auth/google">
                                                <img className='google-img' src={googleIcon} alt='loading'/>
                                                <p className='google-text'> Войти с помощью Google</p>
                                            </a>
                                        </div>
                                </div>
                                <div className="card-footer">
                                    <div className="d-flex justify-content-center links">
                                        <Link to="/signUp" className="link-to-signUp">Зарегистироваться</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {dataError && openError &&
                        <CustomAlert
                            handleCloseSnackBar={this.handleCloseSnackBar}
                            text={'Пользователь с такими данными не существует, пройдите'}
                            linkText={'регитсрацию'}
                            linkTo={'/signUp'}
                        />
                    }
                    {userDoesNotExist && openError &&
                    <CustomAlert
                        handleCloseSnackBar={this.handleCloseSnackBar}
                        text={'Пользователь с такими данными не существует, пройдите'}
                        linkText={'регитсрацию'}
                        linkTo={'/signUp'}
                    />
                    }

                </div>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        login: state.authReducer,
        error: state.authReducer.error,
        authUser: state.authReducer.authUser,
    };
}

const mapDispatchToProps = {
    logIn,
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
