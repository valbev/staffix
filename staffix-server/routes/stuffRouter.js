import {
    createStuff,
    createLocation,
    getAllLocation,
    putItemInLocation,
    getAllItemLocation,
} from "../controllers/stuffController";

var express = require('express');
var router = express.Router();
import {
    createCategory,
    getAllCategory,
} from "../controllers/categoryController";


// stuff Routes

router.post('/category', createCategory);
router.get('/category', getAllCategory);
router.post('/stuff', createStuff);
router.post('/location', createLocation);
router.get('/location', getAllLocation);
router.post('/item/location', putItemInLocation);
router.get('/item/location', getAllItemLocation);
module.exports = router;