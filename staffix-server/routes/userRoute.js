import {errorMessage, status, successMessage} from "../helpers/status";

var express = require('express');
var router = express.Router();
const passport = require('passport');

import { createUser, siginUser } from '../controllers/usersController';
import verifyAuth from "../middleware/verifyAuth";

import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();


// users Routes
router.post('/auth/signup', createUser);
router.post('/auth/signin', siginUser);


router.get('/', (req,res) => {
    try {
        const { headers: authorization } = req;
        const token = authorization.authorization;
        if (!token) {
            errorMessage.error = 'Token not provided';
            return res.status(status.bad).send(errorMessage);
        }
        const decoded =  jwt.verify(token, process.env.SECRET);
        const user = {
            email: decoded.email,
            user_id: decoded.user_id,
            is_admin: decoded.is_admin,
            first_name: decoded.first_name,
            last_name: decoded.last_name,
        };
        successMessage.data = user;
        successMessage.data.token = token;
        return res.status(status.success).send(successMessage);
    } catch (err) {
        errorMessage.error = 'Authentication Failed';
        return res.status(status.unauthorized).send(errorMessage);
    }

});

router.get('/auth/google',
    passport.authenticate('google', {
        session: false,
        scope: ['https://www.googleapis.com/auth/plus.login', 'email']
    }));

router.get('/google/callback',
    passport.authenticate('google', { session: false }),
    function (req, res) {
    res.redirect( 307, `http://localhost:3000/trip/?token=${req.user.token}`);
});


module.exports = router;