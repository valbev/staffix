import {
    setTripStatus,
    createTrip,
    deleteTrip,
    filterTripByDestination,
    getAllTrips
} from "../controllers/tripController";

var express = require('express');
var router = express.Router();
import verifyAuth from "../middleware/verifyAuth";


// trip Routes

router.post('/trips', createTrip);
router.get('/trips', getAllTrips);
router.delete('/trips', deleteTrip);
router.put('/trips/:tripId/:tripStatus', setTripStatus);
router.get('/trips/destinatiovan', verifyAuth, filterTripByDestination);

module.exports = router;