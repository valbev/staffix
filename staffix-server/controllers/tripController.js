import moment from 'moment';
import dbQuery from '../database/dev/dbQuery';

import {
    isEmpty, empty,
} from '../helpers/validations';


import {
    errorMessage, successMessage, status,
} from '../helpers/status';
import jwt from "jsonwebtoken";


const createTrip = async (req, res) => {
    const {
        user_id,
        title,
        planedDispatchDate,
        startPoint,
        destinationPoint,
        note,
    } = req.body;

    const created_on = moment(new Date());
    const planedArriveDate = moment(new Date());

    if (isEmpty(title) || isEmpty(planedDispatchDate) || empty(startPoint) || empty(destinationPoint)) {
        errorMessage.error = 'Title, planedDispatchDate, planedArriveDate, startPint and destinationPoint  field cannot be empty';
        return res.status(status.bad).send(errorMessage);
    }
    const createTripQuery = `INSERT INTO
          trip(user_id, title, planedDispatchDate, planedArriveDate, startPoint, destinationPoint, note, created_on)
          VALUES($1, $2, $3, $4, $5, $6, $7, $8)
          returning *`;

    const values = [
        user_id,
        title,
        planedDispatchDate,
        planedArriveDate,
        startPoint,
        destinationPoint,
        note,
        created_on,
    ];

    try {
        const { rows } = await dbQuery.query(createTripQuery, values);
        const dbResponse = rows[0];
        successMessage.data = dbResponse;
        return res.status(status.created).send(successMessage);
    } catch (error) {
        console.log(error);
        errorMessage.error = 'Unable to create trip';
        return res.status(status.error).send(errorMessage);
    }
};


/**
 * Delete Trip by ID
 */
const deleteTrip = async (req, res) => {
    const { tripId } = req.body;
    const user_id = getUserFromToken(req).user_id;
    const deleteTripQuery = 'DELETE FROM trip WHERE travelId=$1 AND user_id = $2 returning *';
    try {
        const { rows } = await dbQuery.query(deleteTripQuery, [tripId, user_id]);
        const dbResponse = rows[0];
        if (!dbResponse) {
            errorMessage.error = 'You have no trip with that id';
            return res.status(status.notfound).send(errorMessage);
        }
        successMessage.data = {};
        successMessage.data.message = 'Trip deleted successfully';
        return res.status(status.success).send(successMessage);
    } catch (error) {
        console.log(error);
        return res.status(status.error).send(error);
    }
}


const getUserFromToken = (req) => {
    const { headers: authorization } = req;
    const token = authorization.authorization;
    const decoded =  jwt.verify(token, process.env.SECRET);
    const user = {
        email: decoded.email,
        user_id: decoded.user_id,
        is_admin: decoded.is_admin,
        first_name: decoded.first_name,
        last_name: decoded.last_name,
    };
    return user;
}


/**
 * Get All Trips
 */
const getAllTrips = async (req, res) => {
    const id = getUserFromToken(req).user_id;
    const getAllTripsQuery = 'SELECT trip.title, trip.travelId, trip.planedDispatchDate,' +
        'trip.planedArriveDate, trip.startPoint, trip.destinationPoint, trip.note, ' +
        'trip.status, trip.user_id, start_location.title AS start_point_name, destination_location.title AS destination_point_name FROM trip LEFT JOIN ' +
        'location AS start_location ON trip.startPoint = start_location.locationId ' +
        'LEFT JOIN location AS destination_location ON trip.destinationPoint = destination_location.locationId ' +
        'WHERE trip.user_id=$1 ORDER BY trip.travelId DESC';

    try {
        const { rows } = await dbQuery.query(getAllTripsQuery, [id]);

        const dbResponse = rows;

        console.log(dbResponse)

        if (!dbResponse[0]) {
            errorMessage.error = 'There are no trips';
            return res.status(status.notfound).send(errorMessage);
        }
        successMessage.data = dbResponse;
        return res.status(status.success).send(successMessage);
    } catch (error) {
        console.log(error);
        errorMessage.error = 'Operation was not successful';
        return res.status(status.error).send(errorMessage);
    }
};


/**
 * cancel A Trip
 */
const setTripStatus = async (req, res) => {
    const { tripId, tripStatus } = req.params;
    const id = getUserFromToken(req).user_id;

    const cancelTripQuery = 'UPDATE trip SET status=$1 WHERE travelid=$2 AND user_id=$3 returning *';
    const values = [
        tripStatus,
        Number(tripId),
        id,
    ];
    try {
        const { rows } = await dbQuery.query(cancelTripQuery, values);
        const dbResponse = rows[0];
        if (!dbResponse) {
            errorMessage.error = 'There is no trip with that id';
            return res.status(status.notfound).send(errorMessage);
        }
        successMessage.data = {};
        successMessage.data.message = 'Trip cancelled successfully';
        return res.status(status.success).send(successMessage);
    } catch (error) {
        console.log(error);
        errorMessage.error = 'Operation was not successful';
        return res.status(status.error).send(errorMessage);
    }
};


/**
 * filter trips by destination
 */
const filterTripByDestination = async (req, res) => {
    const { destination } = req.query;
    const findTripQuery = 'SELECT * FROM trip WHERE destination=$1 ORDER BY id DESC';
    try {
        const { rows } = await dbQuery.query(findTripQuery, [destination]);
        const dbResponse = rows;
        if (!dbResponse[0]) {
            errorMessage.error = 'No Trips with that destination';
            return res.status(status.notfound).send(errorMessage);
        }
        successMessage.data = dbResponse;
        return res.status(status.success).send(successMessage);
    } catch (error) {
        errorMessage.error = 'Operation was not successful';
        return res.status(status.error).send(errorMessage);
    }
};

export {
    createTrip,
    getAllTrips,
    setTripStatus,
    filterTripByDestination,
    deleteTrip,
};