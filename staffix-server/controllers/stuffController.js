import moment from 'moment';
import dbQuery from '../database/dev/dbQuery';

import {
    isEmpty, empty,
} from '../helpers/validations';


import {
    errorMessage, successMessage, status, trip_statuses,
} from '../helpers/status';
import jwt from "jsonwebtoken";



const getUserFromToken = (req) => {
    const { headers: authorization } = req;
    const token = authorization.authorization;
    const decoded =  jwt.verify(token, process.env.SECRET);
    const user = {
        email: decoded.email,
        user_id: decoded.user_id,
        is_admin: decoded.is_admin,
        first_name: decoded.first_name,
        last_name: decoded.last_name,
    };
    return user;
}


const createStuff = async (req, res) => {
    const {
        title,
        category_id,
    } = req.body;

    const user_id = getUserFromToken(req).user_id;

    if (isEmpty(title)) {
        errorMessage.error = 'title field cannot be empty';
        return res.status(status.bad).send(errorMessage);
    }
    const createStuffQuery = `INSERT INTO
          stuff(user_id, category_id, title)
          VALUES($1, $2, $3)
          returning *`;
    const values = [
        user_id,
        category_id,
        title,
    ];

    try {
        const { rows } = await dbQuery.query(createStuffQuery, values);
        const dbResponse = rows[0];
        successMessage.data = dbResponse;
        return res.status(status.created).send(successMessage);
    } catch (error) {
        console.log(error);
        errorMessage.error = 'Unable to create category';
        return res.status(status.error).send(errorMessage);
    }
};


const createLocation = async (req, res) => {
    const {
        title,
    } = req.body;

    const user_id = getUserFromToken(req).user_id;

    if (isEmpty(title)) {
        errorMessage.error = 'title field cannot be empty';
        return res.status(status.bad).send(errorMessage);
    }
    const createLocationQuery = `INSERT INTO
          location(user_id, title)
          VALUES($1, $2)
          returning *`;
    const values = [
        user_id,
        title,
    ];

    try {
        const { rows } = await dbQuery.query(createLocationQuery, values);
        successMessage.data = rows[0];
        return res.status(status.created).send(successMessage);
    } catch (error) {
        console.log(error);
        errorMessage.error = 'Unable to create category';
        return res.status(status.error).send(errorMessage);
    }
};

const putItemInLocation = async (req, res) => {
    const {
        itemId,
        locationId,
    } = req.body;

    const user_id = getUserFromToken(req).user_id;
    itemId.map(async (id) => {
        const createItemInLocationQuery = `INSERT INTO
          itemInLocation(user_id, item_id, location_id)
          VALUES($1, $2, $3)
          returning *`;

        const values = [
            user_id,
            id,
            locationId,
        ];
        try {
            await dbQuery.query(createItemInLocationQuery, values);

        } catch (error) {
            console.log(error);
            // errorMessage.error = 'Unable to create category';
            // return res.status(status.error).send(errorMessage);
        }
    })

    return res.status(status.created);
};


const getAllLocation = async (req, res) => {
    const id = getUserFromToken(req).user_id;
    const getAllLocationQuery = 'SELECT * FROM location WHERE user_id=$1 ORDER BY locationId DESC';
    try {
        const { rows } = await dbQuery.query(getAllLocationQuery, [id]);
        const dbResponse = rows;
        if (!dbResponse[0]) {
            errorMessage.error = 'There are no location';
            return res.status(status.notfound).send(errorMessage);
        }
        successMessage.data = dbResponse;
        return res.status(status.success).send(successMessage);
    } catch (error) {
        console.log(error);
        errorMessage.error = 'Operation was not successful';
        return res.status(status.error).send(errorMessage);
    }
};

const getAllItemLocation = async (req, res) => {
    const id = getUserFromToken(req).user_id;
    const getAllLocationQuery = 'SELECT * FROM itemInLocation WHERE user_id=$1 ORDER BY id DESC';
    try {
        const { rows } = await dbQuery.query(getAllLocationQuery, [id]);
        const dbResponse = rows;
        if (!dbResponse[0]) {
            errorMessage.error = 'There are no item in location';
            return res.status(status.notfound).send(errorMessage);
        }
        successMessage.data = dbResponse;
        return res.status(status.success).send(successMessage);
    } catch (error) {
        console.log(error);
        errorMessage.error = 'Operation was not successful';
        return res.status(status.error).send(errorMessage);
    }
};


export {
    createStuff,
    createLocation,
    getAllLocation,
    putItemInLocation,
    getAllItemLocation,
};