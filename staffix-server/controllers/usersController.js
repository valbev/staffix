import moment from 'moment';
import dbQuery from '../database/dev/dbQuery';

import {
    hashPassword,
    comparePassword,
    isValidEmail,
    validatePassword,
    isEmpty,
    generateUserToken,
} from '../helpers/validations';

import {
    errorMessage, successMessage, status,
} from '../helpers/status';

/**
 Create user with some fields and validations
 */
const createUser = async (req, res) => {
    const { email, first_name, last_name, password } = req.body;
    const created_on = moment(new Date());
    const login_type = 'login';

    if (isEmpty(email) || isEmpty(first_name) || isEmpty(last_name) || isEmpty(password)) {
        errorMessage.error = 'Email, password, first name and last name field cannot be empty';
        return res.status(status.bad).send(errorMessage);
    }
    if (!isValidEmail(email)) {
        errorMessage.error = 'Please enter a valid Email';
        return res.status(status.bad).send(errorMessage);
    }
    if (!validatePassword(password)) {
        errorMessage.error = 'Password must be more than five(5) characters';
        return res.status(status.bad).send(errorMessage);
    }
    const hashedPassword = hashPassword(password);
    const createUserQuery = `INSERT INTO
      users(email, first_name, last_name, password, created_on, login_type)
      VALUES($1, $2, $3, $4, $5, $6)
      returning *`;
    const values = [
        email,
        first_name,
        last_name,
        hashedPassword,
        created_on,
        login_type,
    ];

    try {
        const { rows } = await dbQuery.query(createUserQuery, values);
        const dbResponse = rows[0];
        delete dbResponse.password;
        const token = generateUserToken(dbResponse.email, dbResponse.id, dbResponse.is_admin, dbResponse.first_name, dbResponse.last_name);
        successMessage.data = dbResponse;
        successMessage.data.token = token;
        return res.status(status.created).send(successMessage);
    } catch (error) {
        if (error.routine === '_bt_check_unique') {
            const findUserQuery = 'SELECT * FROM users WHERE email=$1 AND login_type=$2';
            const { rows } = await dbQuery.query(findUserQuery, [email, 'google']);
            const dbResponse = rows[0];
            if(dbResponse) {
                errorMessage.error = 'This email is already registered with google';
                return res.status(status.conflict).send(errorMessage);
            } else {
                errorMessage.error = 'User with that EMAIL already exist';
                return res.status(status.conflict).send(errorMessage);
            }
        }
        errorMessage.error = 'Operation was not successful';
        return res.status(status.error).send(errorMessage);
    }
};

/**
 SignIn user with generate jwt token and validations
 */
const siginUser = async (req, res) => {
    const { email, password } = req.body;
    if (isEmpty(email) || isEmpty(password)) {
        errorMessage.error = 'Email or Password detail is missing';
        return res.status(status.bad).send(errorMessage);
    }
    if (!isValidEmail(email) || !validatePassword(password)) {
        errorMessage.error = 'Please enter a valid Email or Password';
        return res.status(status.unauthorized).send(errorMessage);
    }
    const signinUserQuery = 'SELECT * FROM users WHERE email = $1';
    try {
        const { rows } = await dbQuery.query(signinUserQuery, [email]);
        const dbResponse = rows[0];

        if (!dbResponse) {
            errorMessage.error = 'User with this email does not exist';
            return res.status(status.notfound).send(errorMessage);
        }
        if(dbResponse.password) {
            if (!comparePassword(dbResponse.password, password)) {
                errorMessage.error = 'The password you provided is incorrect';
                return res.status(status.unauthorized).send(errorMessage);
            }
        } else {
            errorMessage.error = 'The password you provided is incorrect';
            return res.status(status.unauthorized).send(errorMessage);
        }

        const token = generateUserToken(dbResponse.email, dbResponse.id, dbResponse.is_admin, dbResponse.first_name, dbResponse.last_name);
        delete dbResponse.password;
        successMessage.data = dbResponse;
        successMessage.data.token = token;
        return res.status(status.success).send(successMessage);
    } catch (error) {
        console.log(error);
        errorMessage.error = 'Operation was not successful';
        return res.status(status.error).send(errorMessage);
    }
};


const GoogleLogin = (User) => new Promise ((res, rej) => {
    const { _json: { given_name, family_name, email, picture } } = User;
    const created_on = moment(new Date());
    const login_type = 'google';
    const hashedPassword = null;

    const signinUserQuery = 'SELECT * FROM users WHERE email = $1';
    dbQuery.query(signinUserQuery, [email])
        .then((resp) => {
            const dbResponse = resp.rows[0];
            if(dbResponse === undefined || dbResponse.length === 0) {
                const createUserQuery = `INSERT INTO
                  users(email, first_name, last_name, password, created_on, login_type)
                  VALUES($1, $2, $3, $4, $5, $6)
                  returning *`;
                const values = [
                    email,
                    given_name,
                    family_name,
                    hashedPassword,
                    created_on,
                    login_type,
                ];
                dbQuery.query(createUserQuery, values)
                    .then((data) => {
                        const dbResponse = data.rows[0];
                        delete dbResponse.password;
                        const token = generateUserToken(dbResponse.email, dbResponse.id, dbResponse.is_admin, dbResponse.first_name, dbResponse.last_name);
                        const response = { status: status.created, message: successMessage, data: dbResponse, token: token };
                        return res(response);
                    })
                    .catch((error) => console.log('some error from database', error));

            } else {
               delete dbResponse.password;
               const token = generateUserToken(dbResponse.email, dbResponse.id, dbResponse.is_admin, dbResponse.first_name, dbResponse.last_name);
               const response = { status: status.success, message: successMessage, data: dbResponse, token: token};
               return res(response);
            }
        });
});

export {
    createUser,
    siginUser,
    GoogleLogin,
};