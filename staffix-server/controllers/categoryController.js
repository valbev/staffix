import moment from 'moment';
import dbQuery from '../database/dev/dbQuery';

import {
    isEmpty, empty,
} from '../helpers/validations';


import {
    errorMessage, successMessage, status, trip_statuses,
} from '../helpers/status';
import jwt from "jsonwebtoken";


const getUserFromToken = (req) => {
    const { headers: authorization } = req;
    const token = authorization.authorization;
    const decoded =  jwt.verify(token, process.env.SECRET);
    const user = {
        email: decoded.email,
        user_id: decoded.user_id,
        is_admin: decoded.is_admin,
        first_name: decoded.first_name,
        last_name: decoded.last_name,
    };
    return user;
}

const createCategory = async (req, res) => {
    const {
        title,
    } = req.body;

    const user_id = getUserFromToken(req).user_id;

    if (isEmpty(title)) {
        errorMessage.error = 'Title field cannot be empty';
        return res.status(status.bad).send(errorMessage);
    }
    const createCategoryQuery = `INSERT INTO
          category(user_id, title)
          VALUES($1, $2)
          returning *`;
    const values = [
        user_id,
        title,
    ];

    try {
        const { rows } = await dbQuery.query(createCategoryQuery, values);
        const dbResponse = rows[0];
        successMessage.data = dbResponse;
        return res.status(status.created).send(successMessage);
    } catch (error) {
        console.log(error);
        errorMessage.error = 'Unable to create category';
        return res.status(status.error).send(errorMessage);
    }
};


function groupById(arr) {
    //свертка массива во временный объект, с датами в качестве ключей
    const temp = arr.reduce((acc, elem) => {
       let date = elem.categoryid;
        //если ключа-даты еще нет в объекте, записываем туда пустой массив
        if(!acc[date]) {
            acc[date] = [];
        }
        // ложим текущий элемент в соответствующий массив
        acc[date].push(elem);
        return acc;
    }, {});
    // извлекаем все ключи получившегося объекта в массив
    // и преобразуем массив ключей в массив значений
    return Object.getOwnPropertyNames(temp).map(k => temp[k]);
}


const getAllCategory = async (req, res) => {
    const id = getUserFromToken(req).user_id;
    const getCategory = 'SELECT category.title, category.user_id, category.categoryId, stuff.title AS stuff_name, ' +
        'stuff.ItemId FROM category LEFT JOIN stuff ON category.categoryId = stuff.category_id ' +
        'WHERE category.user_id = $1 ORDER BY categoryId DESC';

    const { rows } = await dbQuery.query(getCategory, [id]);

    const value = [];

    groupById(rows).map((item) => {
        const stuffs = [];
        item.forEach((category) => {
            stuffs.push({stuff_name: category.stuff_name, id: category.itemid})
        })
        const values = {
            title: item[0].title,
            user_id: item[0].user_id,
            category_id: item[0].categoryid,
            stuff: stuffs,
        }
        value.push(values);
    })

    try {
        const dbResponse = value;
        if (!dbResponse[0]) {
            errorMessage.error = 'There are no trips';
            return res.status(status.notfound).send(errorMessage);
        }
        successMessage.data = dbResponse;
        return res.status(status.success).send(successMessage);
    } catch (error) {
        console.log(error);
        errorMessage.error = 'Operation was not successful';
        return res.status(status.error).send(errorMessage);
    }
};

export {
    createCategory,
    getAllCategory,
};