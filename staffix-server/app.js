let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');

let passport = require('passport');
let GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

const GOOGLE_CLIENT_SECRET='EDJYFB9NeyBBmroKy91JCWHU';
const GOOGLE_CLIENT_ID='644659366229-1sj62kma1fhjj25ckcvnq6rsbbu3tb1l.apps.googleusercontent.com';
const CLIENT_APP ='http://localhost:3001';

const { GoogleLogin } = require('./controllers/usersController');


import 'babel-polyfill';
import cors from 'cors';
import env from './env';

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/userRoute');
let tripRoute = require('./routes/tripRoute');
let stuffRoute = require('./routes/stuffRouter');


let app = express();

app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//google
passport.use(new GoogleStrategy({
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: `${CLIENT_APP}`+'/api/v1/google/callback',
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
         let user = await GoogleLogin(profile);
         return done(null, user);
      } catch (e) {
        done(e);
      }
    }
));


app.use('/', indexRouter);
app.use('/api/v1', usersRouter);
app.use('/api/v1', tripRoute);
app.use('/api/v1', stuffRoute);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
