import pool from './pool';

pool.on('connect', () => {
    console.log('connected to the db');
});

/**
 * Create User Table
 * CREATE TABLE test
 (id SERIAL PRIMARY KEY,
 name VARCHAR(100) UNIQUE NOT NULL,
 phone VARCHAR(100));
 */

const createUserTable = () => {
    const userCreateQuery = `CREATE TABLE IF NOT EXISTS users
      (id SERIAL PRIMARY KEY,
      google_id VARCHAR(100) NULL,
      email VARCHAR(100) UNIQUE NOT NULL,
      first_name VARCHAR(100),
      last_name VARCHAR(100),
      password VARCHAR(100) NULL,
      login_type VARCHAR(100) NULL,
      created_on DATE NOT NULL)`;

    pool.query(userCreateQuery)
        .then((res) => {
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};


/**
 * Create Trip Table
 */
const createTripTable = () => {
    const tripCreateQuery = `CREATE TABLE IF NOT EXISTS trip
    (travelId SERIAL PRIMARY KEY, 
    title VARCHAR(300) NOT NULL,
    planedDispatchDate DATE NOT NULL,
    planedArriveDate DATE NOT NULL,
    actualArriveDare DATE NULL,
    actualDispatchDate DATE NULL,
    startPoint INTEGER REFERENCES location(locationId) ON DELETE CASCADE,
    destinationPoint INTEGER REFERENCES location(locationId) ON DELETE CASCADE,
    note VARCHAR(400) NULL,
    status float DEFAULT(0.00),
    user_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
    created_on DATE NOT NULL)`;

    pool.query(tripCreateQuery)
        .then((res) => {
            pool.end();
        })
        .catch((err) => {
            pool.end();
        });
};



const createCategoryTable = () => {
    const tripCreateQuery = `CREATE TABLE IF NOT EXISTS category
    (categoryId SERIAL PRIMARY KEY, 
    title VARCHAR(300) NOT NULL,
    user_id INTEGER REFERENCES users(id) ON DELETE CASCADE)`;

    pool.query(tripCreateQuery)
        .then((res) => {
            pool.end();
        })
        .catch((err) => {
            pool.end();
        });
};


const createStuffTable = () => {
    const tripCreateQuery = `CREATE TABLE IF NOT EXISTS stuff
    (itemId SERIAL PRIMARY KEY, 
    title VARCHAR(300) NOT NULL,
    category_id INTEGER REFERENCES category(categoryId) ON DELETE CASCADE,
    user_id INTEGER REFERENCES users(id) ON DELETE CASCADE)`;

    pool.query(tripCreateQuery)
        .then((res) => {
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};

const creatItemInLocationTable = () => {
    const itemInLocationQuery = `CREATE TABLE IF NOT EXISTS itemInLocation
    (id SERIAL PRIMARY KEY,
    item_id INTEGER REFERENCES stuff(itemId) ON DELETE CASCADE,
    location_id INTEGER REFERENCES location(locationId) ON DELETE CASCADE,
    qty INTEGER NULL,
    user_id INTEGER REFERENCES users(id) ON DELETE CASCADE)`;

    pool.query(itemInLocationQuery)
        .then((res) => {
            pool.end();
        })
        .catch((err) => {
            pool.end();
        });
};

const creatLocationTable = () => {
    const locationQuery = `CREATE TABLE IF NOT EXISTS location
    (locationId SERIAL PRIMARY KEY, 
    title VARCHAR(300) NOT NULL,
    description VARCHAR(400) NULL,
    archived VARCHAR(400) NULL,
    address VARCHAR(400) NULL,
    user_id INTEGER REFERENCES users(id) ON DELETE CASCADE)`;

    pool.query(locationQuery)
        .then((res) => {
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};



/**
 * Drop User Table
 */
const dropUserTable = () => {
    const usersDropQuery = 'DROP TABLE IF EXISTS users';
    pool.query(usersDropQuery)
        .then((res) => {
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};



/**
 * Drop Trip Table
 */
const dropTripTable = () => {
    const tripDropQuery = 'DROP TABLE IF EXISTS trip';
    pool.query(tripDropQuery)
        .then((res) => {
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};


/**
 * Create All Tables
 */
const createAllTables = () => {
    createUserTable();
    createTripTable();
    createCategoryTable();
    createStuffTable();
    creatLocationTable();
    creatItemInLocationTable();
};


/**
 * Drop All Tables
 */
const dropAllTables = () => {
    dropUserTable();
    dropTripTable();

};

pool.on('remove', () => {
    process.exit(0);
});


export {
    createAllTables,
    dropAllTables,
};

require('make-runnable');